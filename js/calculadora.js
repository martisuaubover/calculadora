/* _________________________________________________________________________________________________________________

====================================================================================================================
                                                EXPLICACIONES
====================================================================================================================

1. Print
    Función para introducir en pantalla los operadores y operandos, haciendo que se puedan poner correlativos si
    pasan el primer if y de esta forma, modificando el operando.
    El otro if es para trasladar el contenido de entryNumber a exitNumber al usar un operador.

    1.1 Zero limitation
        El primer if evita que se puedan escribir "0" a nivel plural si no hay nada escrito previo,
        limitándolo a un único "0" (para poder usarse el recurso del decimal).

        El segundo if sirve para que, si se escribe un "0" al principio pero en vez de usar luego el ".", se
        escribe otro número, el 0 desaparecerá.

2. Calculation
    Función para llevar a cabo la operación (base de la calculadora), uniendo los dos elementos de ambas pantallas
    y generar el cálculo con eval. Luego, mostramos a la pantalla de entrada el resultado y limpiamos la de salida.

3. Delete last entry
    Función para poder borrar el último botón apretado en la calculadora, ya sea operando u operador. Utilizamos
    el método substring para darle el nuevo valor sin la última entrada y luego le damos ese valor a la variante
    que le dará el input en entryNumber.
    Aprovechamos el mismo "operando" para modificarlo y reasignarlo con el mismo nombre en una segunda variable.

4. Clear entry
    Función que limpia únicamente todo lo que hay en entryNumber.

5. Clear all
    Función que limpia todo el texto que hay en ambos párrafos de la calculadora.

__________________________________________________________________________________________________________________ */

// Print 
function print(value){
    
    var operando = document.getElementById("entryNumber").innerHTML;
    var operador = ['+' , '-' , '*' , '/']; // Cambiar esto en una solución mejor a que sea más fácil añadir nuevos operadores.

    if(operando == ''){
        document.getElementById("entryNumber").innerHTML = value;

    }else{   
        operando += value;
        document.getElementById("entryNumber").innerHTML = operando;
    }

    if(operador.includes(value)){ //podría tener uso en Simon
        document.getElementById("exitNumber").innerHTML = operando;
        document.getElementById("entryNumber").innerHTML = '';       
    }
    
    // Zero limitation
    if(value == "0" && operando.substring(0 , 1) == "0" && operando.substring(1, 2) != "."){ 
        var operando = operando.substring(0 , operando.length - 1);
        document.getElementById("entryNumber").innerHTML = operando;
    }

    if(operando.substring(0,2) != "0."){
        if(value != "0" && operando.substring(0 , 1) == "0"){
            var substitution = operando.replace("0", value);
            var operando = operando.substring(1);
            document.getElementById("entryNumber").innerHTML = operando;  
        }
    }

    // Dot limitation
    if(value == "." && operando.substring(0 , 1)== ""){
        document.getElementById("entryNumber").innerHTML = "0.";
    }
    
    /*if(operando.indexOf(".") > -1){
        value += "";
        document.getElementById("entryNumber").innerHTML = value;

    }else if(operando.indexOf(".") == -1){
        value += ".";
        document.getElementById("entryNumber").innerHTML = value;
    }*/

    /*if(value == "." && operando.includes(".")){ //Hay que referirse a múltiples puntos
        //var operando = operando.substring(0, operando.length - 1);
        var n = operando.indexOf(".") > -1;
        alert(n);
        alert(operando.indexOf(".") == -1);*/
        //var operando = operando.substring(n -1);
        //alert(operando);
        //document.getElementById("entryNumber").innerHTML = operando;
//}

    /*if(operando.includes(".")){ //Hay que referirse a múltiples puntos
        var guatemala = ".";
        value = guatemala;
    }*/
}

// Calculation 
function calculation(){

    var fusion = document.getElementById("exitNumber").innerHTML + document.getElementById("entryNumber").innerHTML;
    var result = eval(fusion);
    document.getElementById("entryNumber").innerHTML = result;
    document.getElementById("exitNumber").innerHTML = '';

}

// Delete last entry 
function del(){

    var operando = document.getElementById("entryNumber").innerHTML;
    operando = operando.substring(0, operando.length - 1);
    document.getElementById("entryNumber").innerHTML = operando;

}

// Clear entry
 function clearentry(){

    document.getElementById("entryNumber").innerHTML = '';

 }

 // Clear all
 function clearall(){

    document.getElementById("entryNumber").innerHTML = '';
    document.getElementById("exitNumber").innerHTML = '';

 }